package com.ambre.examenblanc.blog.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class SecurityConfiguration {
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
               .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                .authorizeRequests(authorize -> authorize
                        .antMatchers(HttpMethod.GET, "/articles").permitAll()
                        .antMatchers(HttpMethod.GET, "/articles/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/users").permitAll()
                        .anyRequest().authenticated()
                )
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD",
                        "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }
}
