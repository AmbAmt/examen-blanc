package com.ambre.examenblanc.blog.web.articles;

public record CreateArticlesDto(String title,String content, String summary,String tags, String userEmail ) {

    public static class ArticleNotFoundException extends Exception {
        public ArticleNotFoundException(){super("This article dose not exist.");}
    }
}
