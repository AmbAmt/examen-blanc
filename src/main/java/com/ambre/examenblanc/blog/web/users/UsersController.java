package com.ambre.examenblanc.blog.web.users;

import com.ambre.examenblanc.blog.application.UsersManager;
import com.ambre.examenblanc.blog.domain.model.articles.Articles;
import com.ambre.examenblanc.blog.domain.model.users.Users;
import com.ambre.examenblanc.blog.web.articles.CreateArticlesDto;
import org.apache.catalina.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersManager usersManager;

    UsersController(UsersManager usersManager){
        this.usersManager = usersManager;
    }

    @PostMapping
    ResponseEntity<Users> createArticle(@RequestBody CreateUsersDto usersDto){
        return ResponseEntity.ok(usersManager.createUser(usersDto.username(), usersDto.email(), usersDto.password()));
    }
}
