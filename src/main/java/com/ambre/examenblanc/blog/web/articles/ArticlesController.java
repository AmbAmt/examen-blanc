package com.ambre.examenblanc.blog.web.articles;

import com.ambre.examenblanc.blog.application.ArticlesManager;
import com.ambre.examenblanc.blog.domain.model.articles.Articles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    private final ArticlesManager articlesManager;

    @Autowired
    public ArticlesController(ArticlesManager articlesManager){
        this.articlesManager = articlesManager;
    }

    @PostMapping
    ResponseEntity<Articles> createArticle(@RequestBody CreateArticlesDto articlesDto){
        return ResponseEntity.ok(articlesManager.createArticle(Date.valueOf(LocalDate.now()), articlesDto.title(),
                articlesDto.content(), articlesDto.summary(), articlesDto.tags(), articlesDto.userEmail()));
    }

    @GetMapping
    ResponseEntity<Iterable<Articles>> getAllArticles(){
        return ResponseEntity.ok(articlesManager.getAllArticles());
    }

    @GetMapping("/{articleTid}")
    ResponseEntity<Articles> getArticle(@PathVariable String articleTid){
        return articlesManager.getArticle(articleTid).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @PutMapping("/{articleTid}")
    ResponseEntity<Articles> changeArticle(@PathVariable String articleTid, @RequestBody ModifyArticlesDto modifyArticlesDto){
        try {
            Articles articles = articlesManager.changeArticle(articleTid, modifyArticlesDto.content());
            return ResponseEntity.ok(articles);
        } catch (CreateArticlesDto.ArticleNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @DeleteMapping("/{articleTid}")
    @ResponseStatus(HttpStatus.OK)
    void deleteArticle(@PathVariable String articleTid){
        //mettre en place la vérification que l'utilisateur supprimant l'article est bien le créateur
        articlesManager.deleteArticle(articleTid);
    }
}
