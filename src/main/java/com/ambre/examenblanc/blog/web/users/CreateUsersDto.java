package com.ambre.examenblanc.blog.web.users;

public record CreateUsersDto(String username, String email, String password) {
}
