package com.ambre.examenblanc.blog.application;

import com.ambre.examenblanc.blog.domain.model.users.Users;
import com.ambre.examenblanc.blog.domain.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UsersManager {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersManager(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    public Users createUser(String username,String email,String password ){
        Users users = new Users(UUID.randomUUID().toString(), username, email, password);
        usersRepository.save(users);
        return users;
    }

    public Users findByEmail(String email){
        try {
            return usersRepository.findByEmail(email);

        }finally {
            return null;
        }
    }


}
