package com.ambre.examenblanc.blog.application;

import com.ambre.examenblanc.blog.domain.model.articles.Articles;
import com.ambre.examenblanc.blog.domain.model.tags.Tags;
import com.ambre.examenblanc.blog.domain.repository.ArticlesRepository;
import com.ambre.examenblanc.blog.web.articles.CreateArticlesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticlesManager {

    private final ArticlesRepository articlesRepository;
    private final UsersManager usersManager;

    @Autowired
    public ArticlesManager(ArticlesRepository articlesRepository, UsersManager usersManager){
        this.articlesRepository = articlesRepository;
        this.usersManager = usersManager;
    }

    public Articles createArticle(Date date, String title, String content, String summary, String tags, String userEmail){
        //ici il faudra recherger si les tag existent, si ils n'existent pas on les récupèrent
        Articles articles = new Articles(UUID.randomUUID().toString(), date,title,content,summary, tags, usersManager.findByEmail(userEmail));
        articlesRepository.save(articles);
        return articles;
    }

    public Iterable<Articles> getAllArticles(){
        return articlesRepository.findAll();
    }

    public Optional<Articles> getArticle(String articleTid){
       Optional<Articles> articles =  articlesRepository.findById(articleTid);
        return articles;
    }

    public Articles changeArticle(String articleTid,String content) throws CreateArticlesDto.ArticleNotFoundException {
        Articles articles = articlesRepository.findById(articleTid).orElseThrow(CreateArticlesDto.ArticleNotFoundException::new);
        articles.setContent(content);
        articlesRepository.save(articles);
        return articles;
    }

    public void deleteArticle(String articleTid){
        articlesRepository.deleteById(articleTid);
    }
}
