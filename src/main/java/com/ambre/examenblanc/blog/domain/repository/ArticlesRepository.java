package com.ambre.examenblanc.blog.domain.repository;

import com.ambre.examenblanc.blog.domain.model.articles.Articles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticlesRepository extends CrudRepository<Articles, String> {
}
