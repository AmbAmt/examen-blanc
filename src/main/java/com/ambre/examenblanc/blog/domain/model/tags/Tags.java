package com.ambre.examenblanc.blog.domain.model.tags;

import com.ambre.examenblanc.blog.domain.model.articles.Articles;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="tags")
public class Tags {
    @Id
    private String tid;
    private String name;

    @ManyToMany
    @JoinTable( name = "tags_articles",
            joinColumns = @JoinColumn( name = "tags_tid" ),
            inverseJoinColumns = @JoinColumn( name = "articles_tid" ) )
    private List<Articles> articles = new ArrayList<>();
    protected Tags(){
        //for jpa
    }

    public String getName() {
        return name;
    }

    public List<Articles> getArticles() {
        return articles;
    }
}
