package com.ambre.examenblanc.blog.domain.repository;

import com.ambre.examenblanc.blog.domain.model.users.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<Users, String > {
    public Users findByEmail(String email);
}
