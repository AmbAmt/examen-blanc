package com.ambre.examenblanc.blog.domain.model.articles;

import com.ambre.examenblanc.blog.domain.model.tags.Tags;
import com.ambre.examenblanc.blog.domain.model.users.Users;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="articles")
@Access(AccessType.FIELD)
public class Articles {
    @Id
    private String tid;
    private Date date;
    private String title;
    private String content;
    private String summary;

   // @ElementCollection(fetch = FetchType.EAGER)
    //@CollectionTable(name = "tags", joinColumns = @JoinColumn(name="articles_tid"))
    //@Column(name ="name")
    //@OrderColumn(name="tags_tid")
   @ManyToMany
 /*  @JoinTable( name = "tags_articles",
           joinColumns = @JoinColumn( name = "articles_tid" ),
           inverseJoinColumns = @JoinColumn( name = "tags_tid" ) )*/
   @JoinTable( name = "tags_articles",
           joinColumns = @JoinColumn( name = "tags_tid" ),
           inverseJoinColumns = @JoinColumn( name = "articles_tid" ) )
    private List<Tags> tags;

    @ManyToOne
    @JoinColumn(name="users_tid")
    private Users users;

    protected Articles(){
        // for JPA
    }

    public Articles(String tid, Date date, String title, String content, String summary,String tag, Users users) {
        this.tid = tid;
        this.date = date;
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.tags = new ArrayList<>();
        this.users = users;
    }

    public String getTid(){return tid;}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Tags> getTags() {
        return tags;
    }

   /* public void setTags(String tag, String oldTag) {
        this.tags.add(tag);
        this.tags.remove(oldTag);
    }*/

    public Users getUsers(){return users;}

}
