package com.ambre.examenblanc.blog.domain.model.users;

import com.ambre.examenblanc.blog.domain.model.articles.Articles;

import javax.persistence.*;

@Entity
@Table(name="users")
@Access(AccessType.FIELD)
public class Users {
    @Id
    private String tid;
    private String username;
    private String email;
    private String password;

    protected Users(){
        //jor JPA
    }

    public Users(String tid, String username, String email, String password) {
        this.tid = tid;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getTid(){
        return tid;
    }
    public String getUserName() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
