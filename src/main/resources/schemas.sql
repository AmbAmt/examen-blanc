create table if not exists tags(
    tid char(36) primary key,
    name text not null,
    articles_tid char(36) references articles(tid)
);

create table  if not exists articles(
    tid char(36) primary key,
    date date not null,
    title text not null,
    content text not null,
    summary text not null,
    tags_tid char(36) references tags(tid),
    users_tid char(36) references users(tid)
);

create table if not exists users(
    tid char(36) primary key,
    username text not null unique,
    email text not null unique,
    password text not null
);

